**ACADEMIC RESEARCH LIBRARY THYMELEAF**


**Contexto do projecto**

academic-research-library-thymeleaf é uma pequena aplicação desenvolvida em java, com o Spring MVC com a integração do template engine Thymeleaf.

O propósito desta pequena aplicação é demonstrar a estrutura de um projecto Spring MVC com a integração do Thymeleaf, por esta razão, a questão da lógica do negócio, assim como questões de validação da aplicação, não devem ser dados algum tipo de relevância.

A ideia da aplicação é:
- Listar os trabalhos ou pesquisas academicas publicadas na plataforma;
- LisListar os trabalhos ou pesquisas academicas de um determinado Editor(Publisher) publicadas na plataforma:
    - condição: inserir o seu email para pesquisar os seus trabalhos;
- Carregar trabalho ou pesquisa academica na plataforma:
    - condição: inserir o seu email para que lhe seja apresentado o formulário de carregamento;


**Base de dados**

Deve ser criada uma base de dados usando o script que pode ser encontrado no directório /scripts que encontra-se nos resources do projecto;

A não ser que se queira alterar as credencias/dados inicias que vem no projecto, os dados da base de dados são:
- hots: localhost
- db: acr_dev
- username: acr_dev
- password: acr_dev




**Importar projecto**
- Importar o projecto como um projecto Maven já existente, no STS(Spring Tool Suite) ou Eclipse, ainda com o Netbeans;
- Executar o maven clean, pela IDE;
- Executar o maven update pela IDE;



**Executar projecto**
- Executar a class main do projecto que é AcademicResearchLibraryThymeleafApplication.java
- Aceder ao endereço localhos:8080




