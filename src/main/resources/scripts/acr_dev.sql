CREATE DATABASE `acr_dev` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;

-- acr_dev.person definition

CREATE TABLE `person` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(100) NOT NULL,
  `LAST_NAME` varchar(100) NOT NULL,
  `ADDRESS` varchar(100) DEFAULT NULL,
  `PHONE_PRIMARY` varchar(100) DEFAULT NULL,
  `PHONE_SECONDARY` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
);

-- acr_dev.publisher definition

CREATE TABLE `publisher` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `DEGREE` varchar(100) NOT NULL,
  `PERSON_ID` bigint NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `publisher_person_FK` (`PERSON_ID`),
  CONSTRAINT `publisher_person_FK` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`ID`)
);


-- acr_dev.research definition

CREATE TABLE `research` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `APPROVED_DATE` date DEFAULT NULL,
  `APPROVED_BY` varchar(255) NOT NULL,
  `PUBLISHER_ID` bigint NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `research_publisher_FK` (`PUBLISHER_ID`),
  CONSTRAINT `research_publisher_FK` FOREIGN KEY (`PUBLISHER_ID`) REFERENCES `publisher` (`ID`)
);



-- acr_dev.research_type definition

CREATE TABLE `research_type` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `CODE` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
);


-- acr_dev.`user` definition

CREATE TABLE `user` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(100) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `PERSON_ID` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_person_FK` (`PERSON_ID`),
  CONSTRAINT `user_person_FK` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`ID`)
);