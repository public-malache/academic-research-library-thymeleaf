package mz.gov.inage.academic_research_library_thymeleaf.core.util;

import org.hibernate.Hibernate;

public class HibernateUtils {

	public static boolean isInitialized(final Object proxy) {
		return Hibernate.isInitialized(proxy);
	}

}
