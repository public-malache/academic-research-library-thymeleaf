package mz.gov.inage.academic_research_library_thymeleaf.core.aspect;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.NoResultException;

@Service
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS, noRollbackFor = { NoResultException.class,
		EntityNotFoundException.class })
public @interface BusinessQueryService {

}
