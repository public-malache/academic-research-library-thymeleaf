package mz.gov.inage.academic_research_library_thymeleaf.core.integ.dto;

import org.apache.commons.lang3.builder.CompareToBuilder;

public abstract class AbstractDTO implements Comparable<AbstractDTO> {

	private Long id;

	public AbstractDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int compareTo(final AbstractDTO other) {
		if (other == null) {
			throw new NullPointerException();
		}
		return new CompareToBuilder().append((this.getId() == null) && (other.getId() == null), false)
				.append(this.getId(), other.getId()).toComparison();
	}

}
