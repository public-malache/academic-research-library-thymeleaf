package mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import mz.gov.inage.academic_research_library_thymeleaf.core.entity.CreatableEntity;
import mz.gov.inage.academic_research_library_thymeleaf.core.integ.dto.AbstractDTO;
import mz.gov.inage.academic_research_library_thymeleaf.core.util.HibernateUtils;

public abstract class DTOTransformer<E extends Object, DTO extends Object> implements IDTOTransformer<E, DTO> {

	public DTO toDTO(E source) {
		if (source == null || !HibernateUtils.isInitialized(source)) {
			return null;
		}

		return this.transformToDTO(source);
	}

	protected abstract DTO transformToDTO(E source);

	public E fromDTO(DTO source) {
		if (source == null || !HibernateUtils.isInitialized(source)) {
			return null;
		}

		return this.transformFromDTO(source);

	}

	protected abstract E transformFromDTO(DTO source);

	@Override
	public Set<DTO> toDTOS(final Set<E> sources) {
		return toDTOS(sources, new TreeSet<DTO>());
	}

	@Override
	public Set<DTO> toDTOS(final Set<E> sources, Set<DTO> dtos) {
		if (sources == null || !HibernateUtils.isInitialized(sources)) {
			return Collections.emptySet();
		}

		if (dtos == null) {
			dtos = new TreeSet<>();
		}

		for (E e : sources) {
			DTO dto = this.toDTO(e);

			if (dto != null) {
				dtos.add(dto);
			}
		}

		return dtos;
	}

	@Override
	public Set<E> fromDTOS(final Set<DTO> sources) {
		return fromDTOS(sources, new TreeSet<E>());
	}

	@Override
	public Set<E> fromDTOS(final Set<DTO> sources, Set<E> es) {
		if (sources == null) {
			// Entities expect an empty array when the are no relationships
			return Collections.emptySet();
		}

		if (es == null) {
			es = new TreeSet<>();
		}

		for (DTO vo : sources) {
			E e = this.fromDTO(vo);

			if (e != null) {
				es.add(e);
			}
		}

		return es;
	}

	protected <CE extends CreatableEntity<Long>, CDTO extends AbstractDTO> CDTO setValues(final CE source,
			final CDTO target) {
		target.setId(source.getId());

		return target;
	}

	protected <CE extends CreatableEntity<Long>, CDTO extends AbstractDTO> CE setValues(final CDTO source,
			final CE target) {
		target.setId(source.getId());

		return target;
	}

}
