package mz.gov.inage.academic_research_library_thymeleaf.core.entity;

import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class CreatableEntity<T> extends DomainEntity<T> implements ICreatableEntity<T> {

}
