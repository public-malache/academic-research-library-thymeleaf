package mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer;

import java.util.Set;

public interface IDTOTransformer<E extends Object, DTO extends Object> {

	DTO toDTO(E source);

	Set<DTO> toDTOS(Set<E> sources);

	Set<DTO> toDTOS(Set<E> sources, Set<DTO> dtos);

	E fromDTO(DTO source);

	Set<E> fromDTOS(Set<DTO> sources);

	Set<E> fromDTOS(Set<DTO> sources, Set<E> es);

}
