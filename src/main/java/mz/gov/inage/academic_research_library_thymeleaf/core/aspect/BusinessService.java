package mz.gov.inage.academic_research_library_thymeleaf.core.aspect;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.hibernate.NonUniqueObjectException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.NoResultException;
import jakarta.transaction.SystemException;

@Service
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { SystemException.class, NoResultException.class,
		NonUniqueObjectException.class, EntityNotFoundException.class, DataIntegrityViolationException.class,
		RuntimeException.class, Exception.class })
public @interface BusinessService {
	String value() default "";
}
