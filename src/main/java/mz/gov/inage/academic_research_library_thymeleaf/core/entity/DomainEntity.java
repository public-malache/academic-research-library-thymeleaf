package mz.gov.inage.academic_research_library_thymeleaf.core.entity;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class DomainEntity<T> implements IDomainEntity<T> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private T id;

	@Override
	public T getId() {
		return this.id;
	}

	@Override
	public void setId(T id) {
		this.id = id;
	}

}
