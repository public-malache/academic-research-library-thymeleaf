package mz.gov.inage.academic_research_library_thymeleaf.core.entity;

public interface IDomainEntity<T> {

	public T getId();

	public void setId(T id);
}
