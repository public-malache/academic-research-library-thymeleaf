package mz.gov.inage.academic_research_library_thymeleaf.dtos.attachment;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.dto.AbstractDTO;

public class AttachmentResponseDTO extends AbstractDTO {

	private String ownEntity;
	private String fileName;
	private String originalFileName;
	private byte[] file;
	private Long researchId;

	public AttachmentResponseDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

}
