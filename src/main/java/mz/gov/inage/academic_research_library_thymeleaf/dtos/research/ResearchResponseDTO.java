package mz.gov.inage.academic_research_library_thymeleaf.dtos.research;

import java.time.LocalDate;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.dto.AbstractDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.publisher.PublisherResponseDTO;

public class ResearchResponseDTO extends AbstractDTO {

	private String title;
	private String description;
	private LocalDate approvedDated;
	private String approvedBy;
	PublisherResponseDTO publisher;

	public ResearchResponseDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getApprovedDated() {
		return approvedDated;
	}

	public void setApprovedDated(LocalDate approvedDated) {
		this.approvedDated = approvedDated;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public PublisherResponseDTO getPublisher() {
		return publisher;
	}

	public void setPublisher(PublisherResponseDTO publisher) {
		this.publisher = publisher;
	}

}
