package mz.gov.inage.academic_research_library_thymeleaf.dtos.publisher;

import java.util.Set;
import java.util.TreeSet;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.dto.AbstractDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.person.PersonResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchResponseDTO;

public class PublisherResponseDTO extends AbstractDTO {

	private String degree;
	private PersonResponseDTO person;
	Set<ResearchResponseDTO> researches = new TreeSet<>();

	public PublisherResponseDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public PersonResponseDTO getPerson() {
		return person;
	}

	public void setPerson(PersonResponseDTO person) {
		this.person = person;
	}

	public Set<ResearchResponseDTO> getResearches() {
		return researches;
	}

	public void setResearches(Set<ResearchResponseDTO> researches) {
		this.researches = researches;
	}

}
