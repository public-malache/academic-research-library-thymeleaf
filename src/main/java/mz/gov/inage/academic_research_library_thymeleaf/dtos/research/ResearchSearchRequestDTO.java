package mz.gov.inage.academic_research_library_thymeleaf.dtos.research;

public class ResearchSearchRequestDTO {
	private String email;

	public ResearchSearchRequestDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
