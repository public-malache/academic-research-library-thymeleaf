package mz.gov.inage.academic_research_library_thymeleaf.dtos.publisher;

import java.util.Set;
import java.util.TreeSet;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.dto.AbstractDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.person.PersonRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchRequestDTO;

public class PublisherRequestDTO extends AbstractDTO {

	private String degree;
	private PersonRequestDTO person;
	private String password;
	Set<ResearchRequestDTO> researches = new TreeSet<>();

	public PublisherRequestDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public PersonRequestDTO getPerson() {
		return person;
	}

	public void setPerson(PersonRequestDTO person) {
		this.person = person;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<ResearchRequestDTO> getResearches() {
		return researches;
	}

	public void setResearches(Set<ResearchRequestDTO> researches) {
		this.researches = researches;
	}

}
