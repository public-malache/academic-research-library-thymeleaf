package mz.gov.inage.academic_research_library_thymeleaf.dtos.user;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.dto.AbstractDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.person.PersonRequestDTO;

public class UserRequestDTO extends AbstractDTO {

	private Long id;
	private String username;
	private String password;
	private PersonRequestDTO person;

	public UserRequestDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public PersonRequestDTO getPerson() {
		return person;
	}

	public void setPerson(PersonRequestDTO person) {
		this.person = person;
	}

}
