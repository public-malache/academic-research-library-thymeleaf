package mz.gov.inage.academic_research_library_thymeleaf.dtos.user;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.dto.AbstractDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.person.PersonResponseDTO;

public class UserResponseDTO extends AbstractDTO {

	private Long id;
	private String username;
	private String password;
	private PersonResponseDTO person;

	public UserResponseDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public PersonResponseDTO getPerson() {
		return person;
	}

	public void setPerson(PersonResponseDTO person) {
		this.person = person;
	}

}
