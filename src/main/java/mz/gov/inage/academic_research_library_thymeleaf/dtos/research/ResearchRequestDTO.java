package mz.gov.inage.academic_research_library_thymeleaf.dtos.research;

import java.time.LocalDate;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.dto.AbstractDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.publisher.PublisherRequestDTO;

public class ResearchRequestDTO extends AbstractDTO {

	private String title;
	private String description;
	private LocalDate approvedDated;
	private String approvedBy;
	PublisherRequestDTO publisher;

	public ResearchRequestDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getApprovedDated() {
		return approvedDated;
	}

	public void setApprovedDated(LocalDate approvedDated) {
		this.approvedDated = approvedDated;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public PublisherRequestDTO getPublisher() {
		return publisher;
	}

	public void setPublisher(PublisherRequestDTO publisher) {
		this.publisher = publisher;
	}

}
