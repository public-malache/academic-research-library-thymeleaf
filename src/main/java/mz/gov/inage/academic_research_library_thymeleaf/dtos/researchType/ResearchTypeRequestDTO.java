package mz.gov.inage.academic_research_library_thymeleaf.dtos.researchType;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.dto.AbstractDTO;

public class ResearchTypeRequestDTO extends AbstractDTO {

	private String code;
	private String description;

	public ResearchTypeRequestDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
