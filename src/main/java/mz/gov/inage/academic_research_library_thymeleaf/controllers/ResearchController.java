package mz.gov.inage.academic_research_library_thymeleaf.controllers;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchSearchRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PublisherEntity;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchEntity;
import mz.gov.inage.academic_research_library_thymeleaf.query_service.IPublisherQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.query_service.IResearchQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.repositories.PublisherRepository;
import mz.gov.inage.academic_research_library_thymeleaf.services.IResearchService;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.publisher.IPublisherRequestTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.publisher.IPublisherResponseTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.research.IResearchRequestTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.research.IResearchResponseTransformer;

@Controller
@RequestMapping(value = "/researches")
public class ResearchController {

	@Autowired
	private IResearchQueryService researchQueryService;

	@Autowired
	private IPublisherQueryService publisherQueryService;

	@Autowired
	private PublisherRepository publisherRepository;

	@Autowired
	private IPublisherResponseTransformer publisherResponseTransformer;

	@Autowired
	private IPublisherRequestTransformer publisherRequestTransformer;

	@Autowired
	private IResearchService researchService;

	@Autowired
	private IResearchResponseTransformer researchResponseTransformer;

	@Autowired
	private IResearchRequestTransformer researchRequestTransformer;

	@GetMapping("")
	public ModelAndView fetchAllResearches() {
		ModelAndView mv = new ModelAndView("research/research-list");

		Set<ResearchEntity> researches = this.researchQueryService.fetchAllResearches();
		Set<ResearchResponseDTO> researchResponseDTOs = this.researchResponseTransformer.toDTOS(researches);
		mv.addObject("researches", researchResponseDTOs);
		return mv;
	}

	@GetMapping("search-form")
	public ModelAndView searchResearchForm(ResearchSearchRequestDTO researchSearchRequestDTO) {
		ModelAndView mv = new ModelAndView("research/research-search-form");
		return mv;
	}

	@PostMapping("search-result")
	public ModelAndView searchResearch(ResearchSearchRequestDTO researchSearchRequestDTO) {
		ModelAndView mv = new ModelAndView("research/research-my-list");

		Set<ResearchEntity> researches = this.researchQueryService
				.fetchByPublisherEmail(researchSearchRequestDTO.getEmail());
		Set<ResearchResponseDTO> researchResponseDTOs = this.researchResponseTransformer.toDTOS(researches);
		mv.addObject("researches", researchResponseDTOs);
		return mv;
	}

	@GetMapping("create-form-one")
	public ModelAndView researchCreateFormOne(ResearchRequestDTO researchRequestDTO) {
		return new ModelAndView("research/research-create-form-one");
	}

	@PostMapping("create-form-two")
	public ModelAndView researchCreateFormTwo(ResearchRequestDTO researchRequestDTO) {
		ModelAndView mv = new ModelAndView("research/research-create-form-two");

		Optional<PublisherEntity> publisherOp = this.publisherRepository
				.findByEmail(researchRequestDTO.getPublisher().getPerson().getEmail());
		if (publisherOp.isEmpty()) {
			System.err.println("===========================");
			return mv;
		} else {
			System.err.println("*********************");
			researchRequestDTO.setPublisher(this.publisherRequestTransformer.toDTO(publisherOp.get()));
			return mv;
		}
	}

	@PostMapping("create")
	public ModelAndView researchCreate(ResearchRequestDTO researchRequestDTO) {
		ResearchEntity research = this.researchService.create(researchRequestDTO);
		return new ModelAndView("redirect:/");

	}
}
