package mz.gov.inage.academic_research_library_thymeleaf.query_service;

import java.util.Set;

import mz.gov.inage.academic_research_library_thymeleaf.entities.PersonEntity;

public interface IPersonQueryService {
	Set<PersonEntity> findAllPerson();

	PersonEntity findById(Long id);

}
