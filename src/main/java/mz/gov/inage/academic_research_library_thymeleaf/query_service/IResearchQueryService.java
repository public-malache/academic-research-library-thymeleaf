package mz.gov.inage.academic_research_library_thymeleaf.query_service;

import java.util.Set;

import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchEntity;

public interface IResearchQueryService {

	Set<ResearchEntity> fetchAllResearches();

	ResearchEntity fetchByIdl(Long id);

	Set<ResearchEntity> fetchByPublisherEmail(String email);

}
