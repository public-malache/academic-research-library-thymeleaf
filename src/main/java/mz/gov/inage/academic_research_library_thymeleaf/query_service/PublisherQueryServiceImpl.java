package mz.gov.inage.academic_research_library_thymeleaf.query_service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import jakarta.persistence.EntityNotFoundException;
import mz.gov.inage.academic_research_library_thymeleaf.core.aspect.BusinessQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PublisherEntity;
import mz.gov.inage.academic_research_library_thymeleaf.repositories.PublisherRepository;

@BusinessQueryService
public class PublisherQueryServiceImpl implements IPublisherQueryService {

	@Autowired
	private PublisherRepository publisherRepository;

	@Override
	public Set<PublisherEntity> findAllPublishers() {
		return this.publisherRepository.findAllPublishers();
	}

	@Override
	public PublisherEntity findById(Long id) {
		return this.publisherRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("Publicador com o ID: " + id + " nao encontrado"));
	}

	@Override
	public PublisherEntity findByEmail(String email) {
		return this.publisherRepository.findByEmail(email)
				.orElseThrow(() -> new EntityNotFoundException("Publicador com o email: " + email + " nao encontrado"));
	}

}
