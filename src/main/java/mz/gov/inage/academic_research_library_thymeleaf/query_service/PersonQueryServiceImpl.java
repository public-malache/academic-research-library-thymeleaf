package mz.gov.inage.academic_research_library_thymeleaf.query_service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import jakarta.persistence.EntityNotFoundException;
import mz.gov.inage.academic_research_library_thymeleaf.core.aspect.BusinessQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PersonEntity;
import mz.gov.inage.academic_research_library_thymeleaf.repositories.PersonRepository;

@BusinessQueryService
public class PersonQueryServiceImpl implements IPersonQueryService {

	@Autowired
	private PersonRepository personRepository;

	@Override
	public Set<PersonEntity> findAllPerson() {
		return this.personRepository.findAllPerson();
	}

	@Override
	public PersonEntity findById(Long id) {
		return this.personRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("Pessoa com o ID: " + id + "nao encontrada"));
	}

}
