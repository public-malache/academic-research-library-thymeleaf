package mz.gov.inage.academic_research_library_thymeleaf.query_service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import jakarta.persistence.EntityNotFoundException;
import mz.gov.inage.academic_research_library_thymeleaf.core.aspect.BusinessQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.entities.UserEntity;
import mz.gov.inage.academic_research_library_thymeleaf.repositories.UserRepository;

@BusinessQueryService
public class UserQueryServiceImpl implements IUserQueryService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public Set<UserEntity> findAllUsers() {
		return this.userRepository.findAllUsers();
	}

	@Override
	public UserEntity findById(Long id) {
		return this.userRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("Utilizador com o ID: " + id + " nao encontrado."));
	}

}
