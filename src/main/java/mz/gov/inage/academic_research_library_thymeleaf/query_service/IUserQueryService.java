package mz.gov.inage.academic_research_library_thymeleaf.query_service;

import java.util.Set;

import mz.gov.inage.academic_research_library_thymeleaf.entities.UserEntity;

public interface IUserQueryService {
	Set<UserEntity> findAllUsers();

	UserEntity findById(Long id);
}
