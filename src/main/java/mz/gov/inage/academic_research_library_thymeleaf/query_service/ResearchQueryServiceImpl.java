package mz.gov.inage.academic_research_library_thymeleaf.query_service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import jakarta.persistence.EntityNotFoundException;
import mz.gov.inage.academic_research_library_thymeleaf.core.aspect.BusinessQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchEntity;
import mz.gov.inage.academic_research_library_thymeleaf.repositories.ResearchRepository;

@BusinessQueryService
public class ResearchQueryServiceImpl implements IResearchQueryService {

	@Autowired
	private ResearchRepository researchRepository;

	@Override
	public Set<ResearchEntity> fetchAllResearches() {
		return this.researchRepository.fetchAllResearches();
	}

	@Override
	public Set<ResearchEntity> fetchByPublisherEmail(String email) {
		return this.researchRepository.fetchResearchByPublisherEmail(email);
	}

	@Override
	public ResearchEntity fetchByIdl(Long id) {
		return this.researchRepository.fetchResearchById(id)
				.orElseThrow(() -> new EntityNotFoundException("Pesquisa com o ID " + id + " nao encontrada."));
	}

}
