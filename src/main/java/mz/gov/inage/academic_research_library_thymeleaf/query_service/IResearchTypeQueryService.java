package mz.gov.inage.academic_research_library_thymeleaf.query_service;

import java.util.Set;

import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchTypeEntity;

public interface IResearchTypeQueryService {
	Set<ResearchTypeEntity> findAllResearchTypes();

	ResearchTypeEntity findById(Long id);
}
