package mz.gov.inage.academic_research_library_thymeleaf.query_service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import jakarta.persistence.EntityNotFoundException;
import mz.gov.inage.academic_research_library_thymeleaf.core.aspect.BusinessQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchTypeEntity;
import mz.gov.inage.academic_research_library_thymeleaf.repositories.ResearchTypeRepository;

@BusinessQueryService
public class ResearchTypeQueryServiceImpl implements IResearchTypeQueryService {

	@Autowired
	private ResearchTypeRepository researchTypeRepository;

	@Override
	public Set<ResearchTypeEntity> findAllResearchTypes() {
		return this.researchTypeRepository.findAllResearchTypes();
	}

	@Override
	public ResearchTypeEntity findById(Long id) {
		return this.researchTypeRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("Tipo de pesquisa com o ID " + id + " nao encontrado."));
	}

}
