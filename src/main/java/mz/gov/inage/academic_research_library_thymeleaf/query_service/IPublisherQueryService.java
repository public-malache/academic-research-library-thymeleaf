package mz.gov.inage.academic_research_library_thymeleaf.query_service;

import java.util.Set;

import mz.gov.inage.academic_research_library_thymeleaf.entities.PublisherEntity;

public interface IPublisherQueryService {
	Set<PublisherEntity> findAllPublishers();

	PublisherEntity findById(Long id);

	PublisherEntity findByEmail(String email);

}
