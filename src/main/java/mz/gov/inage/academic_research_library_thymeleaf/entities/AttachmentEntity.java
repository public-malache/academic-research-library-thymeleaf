package mz.gov.inage.academic_research_library_thymeleaf.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import mz.gov.inage.academic_research_library_thymeleaf.core.entity.CreatableEntity;

@Entity
@Table(name = "attachment")
public class AttachmentEntity extends CreatableEntity<Long> {

	@Column(name = "OWN_ENTITY")
	private String ownEntity;

	@Column(name = "FILE_NAME")
	private String fileName;

	@Column(name = "ORIGINAL_FILE_NAME")
	private String originalFileName;

	@Column(name = "FILE", nullable = false)
	@Lob
	private byte[] file;

	@Column(name = "RESEARCH_ID", nullable = true)
	private Long researchId;

	public AttachmentEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

}
