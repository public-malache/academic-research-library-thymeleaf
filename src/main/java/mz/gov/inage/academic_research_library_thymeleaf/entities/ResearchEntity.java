package mz.gov.inage.academic_research_library_thymeleaf.entities;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import mz.gov.inage.academic_research_library_thymeleaf.core.entity.CreatableEntity;

@Entity
@Table(name = "research")
public class ResearchEntity extends CreatableEntity<Long> {

	@Column(name = "TITLE")
	private String title;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "APPROVED_DATE")
	private LocalDate approvedDated;

	@Column(name = "APPROVED_BY")
	private String approvedBy;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PUBLISHER_ID", nullable = false)
	PublisherEntity publisher;

	public ResearchEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getApprovedDated() {
		return approvedDated;
	}

	public void setApprovedDated(LocalDate approvedDated) {
		this.approvedDated = approvedDated;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public PublisherEntity getPublisher() {
		return publisher;
	}

	public void setPublisher(PublisherEntity publisher) {
		this.publisher = publisher;
	}

}
