package mz.gov.inage.academic_research_library_thymeleaf.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import mz.gov.inage.academic_research_library_thymeleaf.core.entity.CreatableEntity;

@Entity
@Table(name = "research_type")
public class ResearchTypeEntity extends CreatableEntity<Long> {

	@Column(name = "CODE")
	private String code;

	@Column(name = "DESCRIPTION")
	private String description;

	public ResearchTypeEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
