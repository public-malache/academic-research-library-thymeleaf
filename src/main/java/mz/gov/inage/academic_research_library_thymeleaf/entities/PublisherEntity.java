package mz.gov.inage.academic_research_library_thymeleaf.entities;

import java.util.Set;
import java.util.TreeSet;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import mz.gov.inage.academic_research_library_thymeleaf.core.entity.CreatableEntity;

@Entity
@Table(name = "publisher")
public class PublisherEntity extends CreatableEntity<Long> {

	@Column(name = "DEGREE")
	private String degree;

	@OneToOne()
	@JoinColumn(name = "PERSON_ID", referencedColumnName = "ID")
	private PersonEntity person;

	@OneToMany(mappedBy = "publisher", fetch = FetchType.LAZY)
	Set<ResearchEntity> researches = new TreeSet<>();

	public PublisherEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public PersonEntity getPerson() {
		return person;
	}

	public void setPerson(PersonEntity person) {
		this.person = person;
	}

	public Set<ResearchEntity> getResearches() {
		return researches;
	}

	public void setResearches(Set<ResearchEntity> researches) {
		this.researches = researches;
	}

}
