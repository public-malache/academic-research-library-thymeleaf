package mz.gov.inage.academic_research_library_thymeleaf.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import mz.gov.inage.academic_research_library_thymeleaf.core.entity.CreatableEntity;

@Entity
@Table(name = "user")
public class UserEntity extends CreatableEntity<Long> {

	@Column(name = "USERNAME")
	private String username;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "PERSON_ID", insertable = false, updatable = false)
	private String personId;

	@OneToOne()
	@JoinColumn(name = "PERSON_ID", referencedColumnName = "ID")
	private PersonEntity person;

	public UserEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public PersonEntity getPerson() {
		return person;
	}

	public void setPerson(PersonEntity person) {
		this.person = person;
	}

}
