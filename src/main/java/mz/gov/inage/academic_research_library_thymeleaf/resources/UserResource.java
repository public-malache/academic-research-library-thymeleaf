package mz.gov.inage.academic_research_library_thymeleaf.resources;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mz.gov.inage.academic_research_library_thymeleaf.dtos.user.UserRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.user.UserResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.UserEntity;
import mz.gov.inage.academic_research_library_thymeleaf.query_service.IUserQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.services.IUserService;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.user.IUserRequestTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.user.IUserResponseTransformer;

@RestController
@RequestMapping("users")
public class UserResource {

	@Autowired
	private IUserQueryService userQueryService;

	@Autowired
	private IUserService userService;

	@Autowired
	private IUserResponseTransformer userResponseTransformer;

	@Autowired
	private IUserRequestTransformer userRequestTransformer;

	@GetMapping("find-all")
	public ResponseEntity<Set<UserResponseDTO>> findAllUsers() {
		Set<UserEntity> users = this.userQueryService.findAllUsers();
		Set<UserResponseDTO> usersDTOs = this.userResponseTransformer.toDTOS(users);
		return ResponseEntity.ok(usersDTOs);
	}

	@GetMapping("find-by-id/{id}")
	public ResponseEntity<UserResponseDTO> findUserById(@PathVariable("id") Long id) {
		UserEntity user = this.userQueryService.findById(id);
		UserResponseDTO userDTO = this.userResponseTransformer.toDTO(user);
		return ResponseEntity.ok(userDTO);
	}

	@PostMapping("create")
	public ResponseEntity<UserResponseDTO> createUser(@RequestBody UserRequestDTO userRequestDTO) {
		UserEntity user = this.userService.create(this.userRequestTransformer.fromDTO(userRequestDTO));
		UserResponseDTO userDTO = this.userResponseTransformer.toDTO(user);
		return ResponseEntity.ok(userDTO);
	}
}
