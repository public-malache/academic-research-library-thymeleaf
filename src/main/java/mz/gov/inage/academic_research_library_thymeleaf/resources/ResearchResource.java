package mz.gov.inage.academic_research_library_thymeleaf.resources;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchEntity;
import mz.gov.inage.academic_research_library_thymeleaf.query_service.IResearchQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.services.IResearchService;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.research.IResearchRequestTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.research.IResearchResponseTransformer;

@RestController
@RequestMapping("researches")
public class ResearchResource {

	@Autowired
	private IResearchQueryService researchQueryService;

	@Autowired
	private IResearchService researchService;

	@Autowired
	private IResearchResponseTransformer researchResponseTransformer;

	@Autowired
	private IResearchRequestTransformer researchRequestTransformer;

	@GetMapping("find-all")
	public ResponseEntity<Set<ResearchResponseDTO>> fetchAllResearches() {
		Set<ResearchEntity> researches = this.researchQueryService.fetchAllResearches();
		Set<ResearchResponseDTO> researchResponseDTOs = this.researchResponseTransformer.toDTOS(researches);
		return ResponseEntity.ok(researchResponseDTOs);
	}

	@GetMapping("create")
	public ResponseEntity<ResearchResponseDTO> createResearch(@RequestBody ResearchRequestDTO researchRequestDTO) {
		ResearchEntity research = this.researchService.create(researchRequestDTO);
		ResearchResponseDTO researchResponseDTO = this.researchResponseTransformer.toDTO(research);
		return ResponseEntity.ok(researchResponseDTO);
	}

	@DeleteMapping("create/{id}")
	public ResponseEntity<?> deleteResearch(@PathVariable("id") Long id) {
		this.researchService.delete(id);
		return ResponseEntity.ok(null);
	}
}
