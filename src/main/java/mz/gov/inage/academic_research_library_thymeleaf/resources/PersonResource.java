package mz.gov.inage.academic_research_library_thymeleaf.resources;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mz.gov.inage.academic_research_library_thymeleaf.dtos.person.PersonRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.person.PersonResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PersonEntity;
import mz.gov.inage.academic_research_library_thymeleaf.query_service.IPersonQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.services.IPersonService;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.person.IPersonRequestTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.person.IPersonResponseTransformer;

@RestController
@RequestMapping("persons")
public class PersonResource {

	@Autowired
	private IPersonQueryService personQueryService;

	@Autowired
	private IPersonService personService;

	@Autowired
	private IPersonResponseTransformer personResponseTransformer;

	@Autowired
	private IPersonRequestTransformer personRequestTransformer;

	@GetMapping("find-all")
	public Set<PersonResponseDTO> findAllPerson() {
		return this.personResponseTransformer.toDTOS(this.personQueryService.findAllPerson());
	}

	@GetMapping("find-by-id/{id}")
	public PersonResponseDTO findPersonById(@PathVariable("id") Long id) {
		PersonEntity person = this.personQueryService.findById(id);
		return this.personResponseTransformer.toDTO(person);
	}

	@PostMapping("create")
	public PersonResponseDTO createPerson(@RequestBody PersonRequestDTO personRequestDTO) {
		PersonEntity person = this.personRequestTransformer.fromDTO(personRequestDTO);
		return this.personResponseTransformer.toDTO(this.personService.create(person));
	}

}
