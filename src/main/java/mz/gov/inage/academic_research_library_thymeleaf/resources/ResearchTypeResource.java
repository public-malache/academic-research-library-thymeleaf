package mz.gov.inage.academic_research_library_thymeleaf.resources;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mz.gov.inage.academic_research_library_thymeleaf.dtos.researchType.ResearchTypeResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchTypeEntity;
import mz.gov.inage.academic_research_library_thymeleaf.query_service.IResearchTypeQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.research_type.IResearchTypeResponseTransformer;

@RestController
@RequestMapping("reaerch-types")
public class ResearchTypeResource {

	@Autowired
	private IResearchTypeQueryService researchTypeQueryService;

	@Autowired
	private IResearchTypeResponseTransformer researchTypeResponseTransformer;

	@GetMapping("find-all")
	public ResponseEntity<Set<ResearchTypeResponseDTO>> findAllResearchTypes() {
		Set<ResearchTypeEntity> researchTypes = this.researchTypeQueryService.findAllResearchTypes();
		Set<ResearchTypeResponseDTO> researchTypeResponseDTOs = this.researchTypeResponseTransformer
				.toDTOS(researchTypes);
		return ResponseEntity.ok(researchTypeResponseDTOs);
	}
}
