package mz.gov.inage.academic_research_library_thymeleaf.resources;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mz.gov.inage.academic_research_library_thymeleaf.dtos.publisher.PublisherRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.publisher.PublisherResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PublisherEntity;
import mz.gov.inage.academic_research_library_thymeleaf.entities.UserEntity;
import mz.gov.inage.academic_research_library_thymeleaf.query_service.IPublisherQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.services.IPublisherService;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.publisher.IPublisherRequestTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.publisher.IPublisherResponseTransformer;

@RestController
@RequestMapping("publishers")
public class PublisherResource {

	@Autowired
	private IPublisherQueryService publisherQueryService;

	@Autowired
	private IPublisherService publisherService;

	@Autowired
	private IPublisherResponseTransformer publisherResponseTransformer;

	@Autowired
	private IPublisherRequestTransformer publisherRequestTransformer;

	@GetMapping("find-all")
	public ResponseEntity<Set<PublisherResponseDTO>> findAllPublishers() {
		Set<PublisherEntity> publishers = this.publisherQueryService.findAllPublishers();
		Set<PublisherResponseDTO> publishersDTOs = this.publisherResponseTransformer.toDTOS(publishers);
		return ResponseEntity.ok(publishersDTOs);
	}

	@GetMapping("find-by-id/{id}")
	public ResponseEntity<PublisherResponseDTO> findPublisherById(@PathVariable("id") Long id) {
		PublisherEntity publisher = this.publisherQueryService.findById(id);
		PublisherResponseDTO publisherDTO = this.publisherResponseTransformer.toDTO(publisher);
		return ResponseEntity.ok(publisherDTO);
	}

	@PostMapping("create")
	public ResponseEntity<PublisherResponseDTO> createPublisher(@RequestBody PublisherRequestDTO publisherRequestDTO) {

		UserEntity user = new UserEntity();
		user.setUsername(publisherRequestDTO.getPerson().getEmail());
		user.setPassword(publisherRequestDTO.getPassword());

		PublisherEntity publisher = this.publisherService.create(publisherRequestDTO);
		PublisherResponseDTO publisherDTO = this.publisherResponseTransformer.toDTO(publisher);
		return ResponseEntity.ok(publisherDTO);
	}
}
