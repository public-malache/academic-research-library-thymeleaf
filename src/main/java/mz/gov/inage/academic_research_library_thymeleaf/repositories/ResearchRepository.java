package mz.gov.inage.academic_research_library_thymeleaf.repositories;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchEntity;

@Repository
public interface ResearchRepository extends JpaRepository<ResearchEntity, Long> {

	@Query("SELECT r FROM ResearchEntity r LEFT JOIN FETCH r.publisher p")
	public Set<ResearchEntity> fetchAllResearches();

	@Query("SELECT r FROM ResearchEntity r LEFT JOIN FETCH r.publisher p WHERE r.id=:id")
	public Optional<ResearchEntity> fetchResearchById(@Param("id") Long id);

	@Query("SELECT r FROM ResearchEntity r LEFT JOIN FETCH r.publisher pu LEFT JOIN FETCH pu.person pr WHERE pr.email=:email")
	public Set<ResearchEntity> fetchResearchByPublisherEmail(@Param("email") String email);

}
