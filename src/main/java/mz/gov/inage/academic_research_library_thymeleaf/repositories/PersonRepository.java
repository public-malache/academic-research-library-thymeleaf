package mz.gov.inage.academic_research_library_thymeleaf.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import mz.gov.inage.academic_research_library_thymeleaf.entities.PersonEntity;

@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, Long> {

	@Query("SELECT p FROM PersonEntity p")
	public Set<PersonEntity> findAllPerson();

}
