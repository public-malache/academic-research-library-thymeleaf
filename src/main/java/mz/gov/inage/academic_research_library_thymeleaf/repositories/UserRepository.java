package mz.gov.inage.academic_research_library_thymeleaf.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import mz.gov.inage.academic_research_library_thymeleaf.entities.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

	@Query("SELECT u FROM UserEntity u")
	public Set<UserEntity> findAllUsers();

}
