package mz.gov.inage.academic_research_library_thymeleaf.repositories;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mz.gov.inage.academic_research_library_thymeleaf.entities.PublisherEntity;

@Repository
public interface PublisherRepository extends JpaRepository<PublisherEntity, Long> {

	@Query("SELECT p FROM PublisherEntity p")
	public Set<PublisherEntity> findAllPublishers();

	@Query("SELECT pu FROM PublisherEntity pu LEFT JOIN pu.person pr WHERE pr.email=:email")
	public Optional<PublisherEntity> findByEmail(@Param("email") String email);

}
