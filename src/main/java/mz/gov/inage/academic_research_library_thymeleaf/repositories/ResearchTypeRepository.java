package mz.gov.inage.academic_research_library_thymeleaf.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchTypeEntity;

@Repository
public interface ResearchTypeRepository extends JpaRepository<ResearchTypeEntity, Long> {

	@Query("SELECT rt FROM ResearchTypeEntity rt")
	Set<ResearchTypeEntity> findAllResearchTypes();

}
