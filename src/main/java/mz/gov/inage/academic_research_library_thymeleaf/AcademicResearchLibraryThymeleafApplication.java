package mz.gov.inage.academic_research_library_thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcademicResearchLibraryThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcademicResearchLibraryThymeleafApplication.class, args);
	}

}
