package mz.gov.inage.academic_research_library_thymeleaf.services;

import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchEntity;

public interface IResearchService {
	ResearchEntity create(ResearchRequestDTO researchRequestDTO);

	void delete(Long id);

}
