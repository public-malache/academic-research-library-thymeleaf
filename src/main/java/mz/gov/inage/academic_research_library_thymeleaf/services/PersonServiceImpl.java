package mz.gov.inage.academic_research_library_thymeleaf.services;

import org.springframework.beans.factory.annotation.Autowired;

import mz.gov.inage.academic_research_library_thymeleaf.core.aspect.BusinessService;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PersonEntity;
import mz.gov.inage.academic_research_library_thymeleaf.repositories.PersonRepository;

@BusinessService
public class PersonServiceImpl implements IPersonService {

	@Autowired
	private PersonRepository personRepository;

	@Override
	public PersonEntity create(PersonEntity person) {
		return this.personRepository.save(person);
	}

}
