package mz.gov.inage.academic_research_library_thymeleaf.services;

import mz.gov.inage.academic_research_library_thymeleaf.entities.PersonEntity;

public interface IPersonService {
	PersonEntity create(PersonEntity person);
}
