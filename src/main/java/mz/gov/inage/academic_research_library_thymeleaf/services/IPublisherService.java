package mz.gov.inage.academic_research_library_thymeleaf.services;

import mz.gov.inage.academic_research_library_thymeleaf.dtos.publisher.PublisherRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PublisherEntity;

public interface IPublisherService {
	PublisherEntity create(PublisherRequestDTO publisherRequestDTO);
}
