package mz.gov.inage.academic_research_library_thymeleaf.services;

import mz.gov.inage.academic_research_library_thymeleaf.entities.UserEntity;

public interface IUserService {
	UserEntity create(UserEntity user);
}
