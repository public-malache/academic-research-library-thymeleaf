package mz.gov.inage.academic_research_library_thymeleaf.services;

import org.springframework.beans.factory.annotation.Autowired;

import mz.gov.inage.academic_research_library_thymeleaf.core.aspect.BusinessService;
import mz.gov.inage.academic_research_library_thymeleaf.entities.UserEntity;
import mz.gov.inage.academic_research_library_thymeleaf.query_service.IPersonQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.repositories.UserRepository;

@BusinessService
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private IPersonQueryService personQueryService;

	@Override
	public UserEntity create(UserEntity user) {
		UserEntity userSaved = this.userRepository.save(user);
		return userSaved;
	}

}
