package mz.gov.inage.academic_research_library_thymeleaf.services;

import org.springframework.beans.factory.annotation.Autowired;

import mz.gov.inage.academic_research_library_thymeleaf.core.aspect.BusinessService;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.publisher.PublisherRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PersonEntity;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PublisherEntity;
import mz.gov.inage.academic_research_library_thymeleaf.entities.UserEntity;
import mz.gov.inage.academic_research_library_thymeleaf.repositories.PublisherRepository;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.person.IPersonRequestTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.publisher.IPublisherRequestTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.user.IUserRequestTransformer;

@BusinessService
public class PublisherServiceImpl implements IPublisherService {

	@Autowired
	private PublisherRepository publisherRepository;

	@Autowired
	private IUserService userService;

	@Autowired
	private IPersonService personService;

	@Autowired
	private IPublisherRequestTransformer publisherRequestTransformer;

	@Autowired
	private IUserRequestTransformer userRequestTransformer;

	@Autowired
	private IPersonRequestTransformer personRequestTransformer;

	@Override
	public PublisherEntity create(PublisherRequestDTO publisherRequestDTO) {

		PublisherEntity publisher = this.publisherRequestTransformer.fromDTO(publisherRequestDTO);
		PersonEntity personEntity = this.personService
				.create(this.personRequestTransformer.fromDTO(publisherRequestDTO.getPerson()));

		publisher.setPerson(personEntity);
		publisher = this.publisherRepository.save(publisher);

		UserEntity user = new UserEntity();
		user.setUsername(publisherRequestDTO.getPerson().getEmail());
		user.setPassword("123456");

		user = this.userService.create(user);

		return publisher;
	}

}
