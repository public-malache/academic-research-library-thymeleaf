package mz.gov.inage.academic_research_library_thymeleaf.services;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import mz.gov.inage.academic_research_library_thymeleaf.core.aspect.BusinessService;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PublisherEntity;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchEntity;
import mz.gov.inage.academic_research_library_thymeleaf.query_service.IResearchQueryService;
import mz.gov.inage.academic_research_library_thymeleaf.repositories.PublisherRepository;
import mz.gov.inage.academic_research_library_thymeleaf.repositories.ResearchRepository;

@BusinessService
public class ResearchServiceImpl implements IResearchService {

	@Autowired
	private ResearchRepository researchRepository;

	@Autowired
	private IResearchQueryService researchQueryService;

	@Autowired
	private IPublisherService publisherService;

	@Autowired
	private PublisherRepository publisherRepository;

	@Override
	public ResearchEntity create(ResearchRequestDTO researchRequestDTO) {

		ResearchEntity research = this.compose(researchRequestDTO);

		Optional<PublisherEntity> publisherOp = this.publisherRepository
				.findByEmail(researchRequestDTO.getPublisher().getPerson().getEmail());

		if (publisherOp.isPresent()) {
			research.setPublisher(publisherOp.get());
		} else {
			PublisherEntity publisher = this.publisherService.create(researchRequestDTO.getPublisher());
			research.setPublisher(publisher);
		}

		return this.researchRepository.save(research);
	}

	@Override
	public void delete(Long id) {
		ResearchEntity research = this.researchQueryService.fetchByIdl(id);
		this.researchRepository.delete(research);
	}

	private ResearchEntity compose(ResearchRequestDTO researchRequestDTO) {
		ResearchEntity research = new ResearchEntity();
		research.setTitle(researchRequestDTO.getTitle());
		research.setDescription(researchRequestDTO.getDescription());
		research.setApprovedBy(researchRequestDTO.getApprovedBy());
		research.setApprovedDated(LocalDate.now());
		return research;
	}

}
