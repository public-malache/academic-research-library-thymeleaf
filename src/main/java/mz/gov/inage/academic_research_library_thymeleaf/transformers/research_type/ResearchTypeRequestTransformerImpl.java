package mz.gov.inage.academic_research_library_thymeleaf.transformers.research_type;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.DTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.Transformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.researchType.ResearchTypeRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchTypeEntity;

@Transformer
public class ResearchTypeRequestTransformerImpl extends DTOTransformer<ResearchTypeEntity, ResearchTypeRequestDTO> {

	@Override
	protected ResearchTypeRequestDTO transformToDTO(ResearchTypeEntity source) {
		ResearchTypeRequestDTO target = setValues(source, new ResearchTypeRequestDTO());
		target.setCode(source.getCode());
		target.setDescription(source.getDescription());
		return target;
	}

	@Override
	protected ResearchTypeEntity transformFromDTO(ResearchTypeRequestDTO source) {
		ResearchTypeEntity target = setValues(source, new ResearchTypeEntity());
		target.setCode(source.getCode());
		target.setDescription(source.getDescription());
		return target;
	}

}
