package mz.gov.inage.academic_research_library_thymeleaf.transformers.person;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.DTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.Transformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.person.PersonRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PersonEntity;

@Transformer
public class PersonRequestTransformerImpl extends DTOTransformer<PersonEntity, PersonRequestDTO>
		implements IPersonRequestTransformer {

	@Override
	protected PersonRequestDTO transformToDTO(PersonEntity source) {
		PersonRequestDTO target = setValues(source, new PersonRequestDTO());
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setAddress(source.getAddress());
		target.setPhonePrimary(source.getPhonePrimary());
		target.setPhoneSecondary(source.getPhoneSecondary());
		target.setEmail(source.getEmail());
		return target;
	}

	@Override
	protected PersonEntity transformFromDTO(PersonRequestDTO source) {
		PersonEntity target = setValues(source, new PersonEntity());
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setAddress(source.getAddress());
		target.setPhonePrimary(source.getPhonePrimary());
		target.setPhoneSecondary(source.getPhoneSecondary());
		target.setEmail(source.getEmail());
		return target;
	}

}
