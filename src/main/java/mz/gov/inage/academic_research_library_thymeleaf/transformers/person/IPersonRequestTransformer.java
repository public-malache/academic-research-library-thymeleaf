package mz.gov.inage.academic_research_library_thymeleaf.transformers.person;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.IDTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.person.PersonRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PersonEntity;

public interface IPersonRequestTransformer extends IDTOTransformer<PersonEntity, PersonRequestDTO> {

}
