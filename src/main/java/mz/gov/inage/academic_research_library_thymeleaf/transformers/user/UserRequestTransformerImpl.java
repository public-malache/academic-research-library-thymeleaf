package mz.gov.inage.academic_research_library_thymeleaf.transformers.user;

import org.springframework.beans.factory.annotation.Autowired;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.DTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.Transformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.user.UserRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.UserEntity;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.person.IPersonRequestTransformer;

@Transformer
public class UserRequestTransformerImpl extends DTOTransformer<UserEntity, UserRequestDTO>
		implements IUserRequestTransformer {

	@Autowired
	private IPersonRequestTransformer personRequestTransformer;

	@Override
	protected UserRequestDTO transformToDTO(UserEntity source) {
		UserRequestDTO target = setValues(source, new UserRequestDTO());
		target.setUsername(source.getUsername());
		target.setPassword(source.getPassword());
		target.setPerson(this.personRequestTransformer.toDTO(source.getPerson()));
		return target;
	}

	@Override
	protected UserEntity transformFromDTO(UserRequestDTO source) {
		UserEntity target = setValues(source, new UserEntity());
		target.setUsername(source.getUsername());
		target.setPassword(source.getPassword());
		target.setPerson(this.personRequestTransformer.fromDTO(source.getPerson()));
		return target;
	}

}
