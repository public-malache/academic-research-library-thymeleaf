package mz.gov.inage.academic_research_library_thymeleaf.transformers.person;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.DTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.Transformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.person.PersonResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PersonEntity;

@Transformer
public class PersonResponseTransformerImpl extends DTOTransformer<PersonEntity, PersonResponseDTO>
		implements IPersonResponseTransformer {

	@Override
	protected PersonResponseDTO transformToDTO(PersonEntity source) {
		PersonResponseDTO target = setValues(source, new PersonResponseDTO());
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setAddress(source.getAddress());
		target.setPhonePrimary(source.getPhonePrimary());
		target.setPhoneSecondary(source.getPhoneSecondary());
		target.setEmail(source.getEmail());
		return target;
	}

	@Override
	protected PersonEntity transformFromDTO(PersonResponseDTO source) {
		PersonEntity target = setValues(source, new PersonEntity());
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setAddress(source.getAddress());
		target.setPhonePrimary(source.getPhonePrimary());
		target.setPhoneSecondary(source.getPhoneSecondary());
		target.setEmail(source.getEmail());
		return target;
	}

}
