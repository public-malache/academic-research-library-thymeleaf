package mz.gov.inage.academic_research_library_thymeleaf.transformers.publisher;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.IDTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.publisher.PublisherResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PublisherEntity;

public interface IPublisherResponseTransformer extends IDTOTransformer<PublisherEntity, PublisherResponseDTO> {

}
