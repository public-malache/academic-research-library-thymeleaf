package mz.gov.inage.academic_research_library_thymeleaf.transformers.user;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.IDTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.user.UserRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.UserEntity;

public interface IUserRequestTransformer extends IDTOTransformer<UserEntity, UserRequestDTO> {

}
