package mz.gov.inage.academic_research_library_thymeleaf.transformers.publisher;

import org.springframework.beans.factory.annotation.Autowired;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.DTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.Transformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.publisher.PublisherResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PublisherEntity;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.person.IPersonResponseTransformer;

@Transformer
public class PublisherResponseTransformerImpl extends DTOTransformer<PublisherEntity, PublisherResponseDTO>
		implements IPublisherResponseTransformer {

	@Autowired
	private IPersonResponseTransformer persIPersonResponseTransformer;

	@Override
	protected PublisherResponseDTO transformToDTO(PublisherEntity source) {
		PublisherResponseDTO target = setValues(source, new PublisherResponseDTO());
		target.setDegree(source.getDegree());
		target.setPerson(this.persIPersonResponseTransformer.toDTO(source.getPerson()));
		return target;
	}

	@Override
	protected PublisherEntity transformFromDTO(PublisherResponseDTO source) {
		PublisherEntity target = setValues(source, new PublisherEntity());
		target.setDegree(source.getDegree());
		target.setPerson(this.persIPersonResponseTransformer.fromDTO(source.getPerson()));
		return target;
	}

}
