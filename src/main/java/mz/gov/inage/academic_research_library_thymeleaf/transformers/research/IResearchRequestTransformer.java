package mz.gov.inage.academic_research_library_thymeleaf.transformers.research;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.IDTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchEntity;

public interface IResearchRequestTransformer extends IDTOTransformer<ResearchEntity, ResearchRequestDTO> {

}
