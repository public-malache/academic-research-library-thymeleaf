package mz.gov.inage.academic_research_library_thymeleaf.transformers.research;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.IDTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchEntity;

public interface IResearchResponseTransformer extends IDTOTransformer<ResearchEntity, ResearchResponseDTO> {

}
