package mz.gov.inage.academic_research_library_thymeleaf.transformers.research;

import org.springframework.beans.factory.annotation.Autowired;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.DTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.Transformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchEntity;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.publisher.IPublisherRequestTransformer;

@Transformer
public class ResearchRequestTransformerImpl extends DTOTransformer<ResearchEntity, ResearchRequestDTO>
		implements IResearchRequestTransformer {

	@Autowired
	private IPublisherRequestTransformer publisherRequestTransformer;

	@Override
	protected ResearchRequestDTO transformToDTO(ResearchEntity source) {
		ResearchRequestDTO target = setValues(source, new ResearchRequestDTO());
		target.setTitle(source.getTitle());
		target.setDescription(source.getDescription());
		target.setApprovedBy(source.getApprovedBy());
		target.setApprovedDated(source.getApprovedDated());
		target.setPublisher(this.publisherRequestTransformer.toDTO(source.getPublisher()));
		return target;
	}

	@Override
	protected ResearchEntity transformFromDTO(ResearchRequestDTO source) {
		ResearchEntity target = setValues(source, new ResearchEntity());
		target.setTitle(source.getTitle());
		target.setDescription(source.getDescription());
		target.setApprovedBy(source.getApprovedBy());
		target.setApprovedDated(source.getApprovedDated());
		target.setPublisher(this.publisherRequestTransformer.fromDTO(source.getPublisher()));
		return target;
	}

}
