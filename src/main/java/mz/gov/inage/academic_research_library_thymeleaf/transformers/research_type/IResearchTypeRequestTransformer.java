package mz.gov.inage.academic_research_library_thymeleaf.transformers.research_type;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.IDTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.researchType.ResearchTypeRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchTypeEntity;

public interface IResearchTypeRequestTransformer extends IDTOTransformer<ResearchTypeEntity, ResearchTypeRequestDTO> {

}
