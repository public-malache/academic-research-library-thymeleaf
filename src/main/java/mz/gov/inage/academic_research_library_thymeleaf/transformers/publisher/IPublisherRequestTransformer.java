package mz.gov.inage.academic_research_library_thymeleaf.transformers.publisher;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.IDTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.publisher.PublisherRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PublisherEntity;

public interface IPublisherRequestTransformer extends IDTOTransformer<PublisherEntity, PublisherRequestDTO> {

}
