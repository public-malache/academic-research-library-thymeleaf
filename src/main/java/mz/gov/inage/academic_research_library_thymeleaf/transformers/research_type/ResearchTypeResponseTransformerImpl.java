package mz.gov.inage.academic_research_library_thymeleaf.transformers.research_type;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.DTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.Transformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.researchType.ResearchTypeResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchTypeEntity;

@Transformer
public class ResearchTypeResponseTransformerImpl extends DTOTransformer<ResearchTypeEntity, ResearchTypeResponseDTO>
		implements IResearchTypeResponseTransformer {

	@Override
	protected ResearchTypeResponseDTO transformToDTO(ResearchTypeEntity source) {
		ResearchTypeResponseDTO target = setValues(source, new ResearchTypeResponseDTO());
		target.setCode(source.getCode());
		target.setDescription(source.getDescription());
		return target;
	}

	@Override
	protected ResearchTypeEntity transformFromDTO(ResearchTypeResponseDTO source) {
		ResearchTypeEntity target = setValues(source, new ResearchTypeEntity());
		target.setCode(source.getCode());
		target.setDescription(source.getDescription());
		return target;
	}

}
