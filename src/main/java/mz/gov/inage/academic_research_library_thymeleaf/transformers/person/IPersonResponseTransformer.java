package mz.gov.inage.academic_research_library_thymeleaf.transformers.person;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.IDTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.person.PersonResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PersonEntity;

public interface IPersonResponseTransformer extends IDTOTransformer<PersonEntity, PersonResponseDTO> {

}
