package mz.gov.inage.academic_research_library_thymeleaf.transformers.research;

import org.springframework.beans.factory.annotation.Autowired;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.DTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.Transformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.research.ResearchResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.ResearchEntity;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.publisher.IPublisherResponseTransformer;

@Transformer
public class ResearchResponseTransformerImpl extends DTOTransformer<ResearchEntity, ResearchResponseDTO>
		implements IResearchResponseTransformer {

	@Autowired
	private IPublisherResponseTransformer publisherResponseTransformer;

	@Override
	protected ResearchResponseDTO transformToDTO(ResearchEntity source) {
		ResearchResponseDTO target = setValues(source, new ResearchResponseDTO());
		target.setTitle(source.getTitle());
		target.setDescription(source.getDescription());
		target.setApprovedBy(source.getApprovedBy());
		target.setApprovedDated(source.getApprovedDated());
		target.setPublisher(this.publisherResponseTransformer.toDTO(source.getPublisher()));
		return target;
	}

	@Override
	protected ResearchEntity transformFromDTO(ResearchResponseDTO source) {
		ResearchEntity target = setValues(source, new ResearchEntity());
		target.setTitle(source.getTitle());
		target.setDescription(source.getDescription());
		target.setApprovedBy(source.getApprovedBy());
		target.setApprovedDated(source.getApprovedDated());
		target.setPublisher(this.publisherResponseTransformer.fromDTO(source.getPublisher()));
		return target;
	}

}
