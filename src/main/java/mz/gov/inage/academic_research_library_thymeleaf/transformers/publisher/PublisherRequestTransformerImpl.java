package mz.gov.inage.academic_research_library_thymeleaf.transformers.publisher;

import org.springframework.beans.factory.annotation.Autowired;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.DTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.Transformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.publisher.PublisherRequestDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.PublisherEntity;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.person.IPersonRequestTransformer;

@Transformer
public class PublisherRequestTransformerImpl extends DTOTransformer<PublisherEntity, PublisherRequestDTO>
		implements IPublisherRequestTransformer {

	@Autowired
	private IPersonRequestTransformer personRequestTransformer;

	@Override
	protected PublisherRequestDTO transformToDTO(PublisherEntity source) {
		PublisherRequestDTO target = setValues(source, new PublisherRequestDTO());
		target.setDegree(source.getDegree());
		target.setPerson(this.personRequestTransformer.toDTO(source.getPerson()));
		return target;
	}

	@Override
	protected PublisherEntity transformFromDTO(PublisherRequestDTO source) {
		PublisherEntity target = setValues(source, new PublisherEntity());
		target.setDegree(source.getDegree());
		target.setPerson(this.personRequestTransformer.fromDTO(source.getPerson()));
		return target;
	}

}
