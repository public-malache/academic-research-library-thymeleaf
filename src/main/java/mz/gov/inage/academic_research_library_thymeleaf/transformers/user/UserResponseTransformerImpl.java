package mz.gov.inage.academic_research_library_thymeleaf.transformers.user;

import org.springframework.beans.factory.annotation.Autowired;

import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.DTOTransformer;
import mz.gov.inage.academic_research_library_thymeleaf.core.integ.transformer.Transformer;
import mz.gov.inage.academic_research_library_thymeleaf.dtos.user.UserResponseDTO;
import mz.gov.inage.academic_research_library_thymeleaf.entities.UserEntity;
import mz.gov.inage.academic_research_library_thymeleaf.transformers.person.IPersonResponseTransformer;

@Transformer
public class UserResponseTransformerImpl extends DTOTransformer<UserEntity, UserResponseDTO>
		implements IUserResponseTransformer {

	@Autowired
	private IPersonResponseTransformer persIPersonResponseTransformer;

	@Override
	protected UserResponseDTO transformToDTO(UserEntity source) {
		UserResponseDTO target = setValues(source, new UserResponseDTO());
		target.setUsername(source.getUsername());
		target.setPassword(source.getPassword());
		target.setPerson(this.persIPersonResponseTransformer.toDTO(source.getPerson()));
		return target;
	}

	@Override
	protected UserEntity transformFromDTO(UserResponseDTO source) {
		UserEntity target = setValues(source, new UserEntity());
		target.setUsername(source.getUsername());
		target.setPassword(source.getPassword());
		target.setPerson(this.persIPersonResponseTransformer.fromDTO(source.getPerson()));
		return target;
	}

}
